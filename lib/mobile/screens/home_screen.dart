import 'package:flutter/material.dart';
import 'package:iium_technician/mobile/components/item_tracker.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';
import 'package:iium_technician/mobile/small_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kDarkBlueColorM,
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Container(
                color: kDarkBlueColorM,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                  child: Row(
                    children: [
                      Expanded(
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 45,
                          child: CircleAvatar(
                            radius: 40,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Hi, Rakan Internet !',
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(height: 8),
                            Text(
                              'Zone 6',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                decoration: BoxDecoration(
                  color: kLightBackGroundColorM,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Container(
                  child: Column(
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'On going task',
                              style: kTextStyleBold(16, Colors.black),
                            ),
                            SizedBox(height: 10),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 20),
                              width: double.infinity,
                              height: 130,
                              decoration: kBoxDecorationM(
                                      radius: 20, color: Colors.white)
                                  .copyWith(
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 10,
                                    color: Colors.grey.withOpacity(0.3),
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Venue',
                                          style: kTextStyleBold(
                                              17, kDarkBlueColorM)),
                                      Text('ICT-LT-C0-01',
                                          style:
                                              kTextStyle(17, kDarkBlueColorM)),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Problem',
                                          style: kTextStyleBold(
                                              17, kDarkBlueColorM)),
                                      Text('Classroom/Lab Projector',
                                          style:
                                              kTextStyle(17, kDarkBlueColorM)),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                  Divider(),
                                  Expanded(
                                    child: Center(
                                      child: Material(
                                        child: InkWell(
                                          onTap: () {
                                            SwitchBody(1).dispatch(context);
                                          },
                                          child: Text('See Details',
                                              style: kTextStyle(
                                                  18, kOrangeColorM)),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 15, top: 10, bottom: 10, right: 5),
                          child: Row(
                            children: [
                              ItemTracker(
                                titleTracker: 'Ticket Accepted',
                                svgSrc: 'assets/svg/ringing.svg',
                                dataTracker: '323',
                              ),
                              ItemTracker(
                                titleTracker: 'Task Completed',
                                svgSrc: 'assets/svg/faq.svg',
                                dataTracker: '43',
                              ),
                              ItemTracker(
                                titleTracker: 'Task Uncompleted',
                                svgSrc: 'assets/svg/exit.svg',
                                dataTracker: '214',
                              ),
                              ItemTracker(
                                titleTracker: 'Test',
                                svgSrc: 'assets/svg/history.svg',
                                dataTracker: '541',
                              ),
                              ItemTracker(
                                titleTracker: 'Test Kedua',
                                svgSrc: 'assets/svg/settings.svg',
                                dataTracker: '435',
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
