import 'package:flutter/material.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';

class TicketAvalaible extends StatefulWidget {
  const TicketAvalaible({ Key? key }) : super(key: key);

  @override
  _TicketAvalaibleState createState() => _TicketAvalaibleState();
}

class _TicketAvalaibleState extends State<TicketAvalaible> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          // Extend the appBar
          Container(
            height: 15,
            width: double.infinity,
            decoration: kBoxDecorationBottomM(0, kDarkBlueColorM),
          ),
          // Title
          Expanded(
            child: Container(
              color: kLightBackGroundColorM,
              width: double.infinity,
              child: Column(
                children: [
                  // Ticket Avalaible
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                      child: Text(
                        'Ticket Avalaible',
                        style: kTextStyleBold(28, Color(0xFF3E81B1)),
                      ),
                    ),
                  ),
                  // Zone
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                      child: Text(
                        '(Zone 6)',
                        style: kTextStyle(16, Color(0xFF242424)),
                      ),
                    ),
                  ),
                  // Ticket Avalaible 1
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                    decoration: kBoxDecorationM(radius: 20, color: Colors.white),
                    width: 440,
                    height: 230,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 250,
                              height: 30,
                              decoration: kBoxDecorationM(radius: 25, color: kDarkestBlueColorM),
                              child: Center(
                                child: Text(
                                  'Classroom / Lab Projector',
                                  style: kTextStyle(16, Colors.white),
                                ),
                              ),
                            ),
                            // Ticket Time
                            SizedBox(
                              width: 40,
                            ),
                            Icon(Icons.timer),
                            Text(
                              '12:32 AM',
                              style: kTextStyle(16, Color(0xFF21365B)),
                            ),
                          ],
                        ),
                        // Ticket Location
                        Row(
                          children: [
                            SizedBox(
                              width: 15,
                            ),
                            Icon(Icons.location_on_outlined, size: 30,),
                            SizedBox(
                              width: 5,
                            ),
                            Text('ICT-LT-C0-01', style: kTextStyle(16, Color(0xFF637FB2)),)
                          ],
                        ),
                        
                        // Ticket Description
                        Row(
                          children: [
                            SizedBox(
                              width: 17,
                            ),
                            Icon(Icons.description_outlined),
                            SizedBox(
                              width: 7,
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                              width: 350,
                              child: Text(
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..', 
                                style: kTextStyle(16, Color(0xFF637FB2)
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        // Accept Button
                        InkWell(
                          onTap: (){},                         
                          child: Container(
                            margin: EdgeInsets.fromLTRB(230, 5, 0, 0),
                            width: 130,
                            height: 40,
                            decoration: kBoxDecorationM(radius: 25, color: Color(0xFFE4FCE5)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('Accept', style: kTextStyle(16, Color(0xFF498760)),),
                                SizedBox(width: 3,),
                                Icon(Icons.add_task, color: Color(0xFF498760),),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Ticket Avalaible 2
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                    decoration: kBoxDecorationM(radius: 20, color: Colors.white),
                    width: 440,
                    height: 230,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.all(15),
                              width: 250,
                              height: 30,
                              decoration: kBoxDecorationM(radius: 25, color: kDarkestBlueColorM),
                              child: Center(
                                child: Text(
                                  'Classroom / Lab Projector',
                                  style: kTextStyle(16, Colors.white),
                                ),
                              ),
                            ),
                            // Ticket Time
                            SizedBox(
                              width: 40,
                            ),
                            Icon(Icons.timer),
                            Text(
                              '12:32 AM',
                              style: kTextStyle(16, Color(0xFF21365B)),
                            ),
                          ],
                        ),
                        // Ticket Location
                        Row(
                          children: [
                            SizedBox(
                              width: 15,
                            ),
                            Icon(Icons.location_on_outlined, size: 30,),
                            SizedBox(
                              width: 5,
                            ),
                            Text('ICT-LT-C0-01', style: kTextStyle(16, Color(0xFF637FB2)),)
                          ],
                        ),
                        
                        // Ticket Description
                        Row(
                          children: [
                            SizedBox(
                              width: 17,
                            ),
                            Icon(Icons.description_outlined),
                            SizedBox(
                              width: 7,
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                              width: 350,
                              child: Text(
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..', 
                                style: kTextStyle(16, Color(0xFF637FB2)
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        // Accept Button
                        InkWell(
                          onTap: (){},
                          child: Container(
                            margin: EdgeInsets.fromLTRB(230, 5, 0, 0),
                            width: 130,
                            height: 40,
                            decoration: kBoxDecorationM(radius: 25, color: Color(0xFFE4FCE5)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('Accept', style: kTextStyle(16, Color(0xFF498760)),),
                                SizedBox(width: 3,),
                                Icon(Icons.add_task, color: Color(0xFF498760),)
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

