import 'package:flutter/material.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';
import 'package:slide_to_confirm/slide_to_confirm.dart';
import 'package:iium_technician/mobile/components/confirmation_dialog.dart';

class TicketScreen extends StatefulWidget {
  const TicketScreen({Key? key}) : super(key: key);

  @override
  _TicketScreenState createState() => _TicketScreenState();
}

class _TicketScreenState extends State<TicketScreen> {
  final List<bool> isSelected = [true, false];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Column(
      children: [
        // Heading for ticket screen
        Expanded(
          child: Container(
            width: double.infinity,
            decoration: kBoxDecorationBottomM(25, kDarkBlueColorM),
            child: Column(
              children: [
                // Venue Name and Zone
                Padding(
                  padding: EdgeInsets.fromLTRB(45, 0, 0, 0),
                  child: Row(
                    children: [
                      Text(
                        'ICT-LR-C0-01',
                        style: kTextStyle(20, kLightBlueColorM),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        '(Zone 6)',
                        style: kTextStyle(17, kLightBlueColorM),
                      ),
                    ],
                  ),
                ),
                // Progress of the ticket
                Padding(
                  padding: EdgeInsets.fromLTRB(45, 5, 0, 0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.circle_rounded,
                        size: 15,
                        color: Color(0xFF96FF45),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'In Progress',
                        style: kTextStyle(16, Colors.white),
                      ),
                    ],
                  ),
                ),
                // Call requester button
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: InkWell(
                    onTap: () {},
                    child: Container(
                      height: 50,
                      width: 350,
                      decoration:
                          kBoxDecorationM(radius: 12, color: kOrangeColorM),
                      child: Center(
                        child: Text(
                          'Call Requester',
                          style: kTextStyle(17, Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // Body for ticket screen
        Expanded(
          flex: 4,
          child: Container(
            width: double.infinity,
            color: kLightBackGroundColorM,
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                // Ticket Detail Box
                Container(
                  height: 120,
                  decoration:
                      kBoxDecorationM(radius: 5, color: kLightBlueColorM),
                  constraints: BoxConstraints(maxWidth: 400),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 15, 0, 0),
                        child: Row(
                          children: [
                            Icon(Icons.settings),
                            SizedBox(
                              width: 5,
                            ),
                            Container(
                              width: 230,
                              height: 30,
                              decoration: kBoxDecorationM(
                                  radius: 30, color: kDarkestBlueColorM),
                              child: Center(
                                child: Text(
                                  'Classroom / Lab Projector',
                                  style: kTextStyle(16, Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                        child: Row(
                          children: [
                            Icon(Icons.description_outlined),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: 270,
                              height: 60,
                              child: Text(
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua..',
                                style: kTextStyle(16, Color(0xFF758EBB)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 30, 5, 0),
                              child: InkWell(
                                onTap: () {},
                                child: Text(
                                  'read more',
                                  style: kTextStyleUnderline(
                                      16, Color(0xFFBB8375)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                // Remark for the ticket
                Container(
                  height: 250,
                  decoration:
                      kBoxDecorationM(radius: 5, color: kLightBlueColorM),
                  constraints: BoxConstraints(
                    maxWidth: 400,
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 5, 0, 5),
                        child: Row(
                          children: [
                            Container(
                              decoration: kBoxDecorationM(
                                  radius: 10, color: Color(0xFFCCE6F8)),
                              height: 40.0,
                              width: 40.0,
                              child: Icon(
                                Icons.info_outline,
                                color: kDarkestBlueColorM,
                                size: 30.0,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Center(
                              child: Text(
                                'Remark',
                                style: kTextStyleBold(20, kDarkestBlueColorM),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                        child: Container(
                          width: 350,
                          height: 100,
                          decoration:
                              kBoxDecorationM(radius: 10, color: Colors.white),
                          child: TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                            controller: remarkController,
                            maxLines: 4,
                          ),
                        ),
                      ),
                      // Toggle Button for complete/uncomplete ticket
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: Container(
                            width: 350,
                            height: 40,
                            decoration: kBoxDecorationM(
                                radius: 10, color: Color(0xFFD4E4EF)),
                            child: Center(
                              child: ToggleButtons(
                                  splashColor: Color(0xFFD4E4EF),
                                  renderBorder: true,
                                  borderColor: Colors.transparent,
                                  borderRadius: BorderRadius.circular(11),
                                  fillColor: Colors.white,
                                  selectedColor: Color(0xFF10124D),
                                  children: <Widget>[
                                    Container(
                                        width: 168,
                                        child:
                                            Center(child: Text('Completed'))),
                                    Container(
                                        width: 168,
                                        child:
                                            Center(child: Text('Uncompleted'))),
                                  ],
                                  isSelected: isSelected,
                                  onPressed: (int index) {
                                    setState(() {
                                      for (int indexBtn = 0;
                                          indexBtn < isSelected.length;
                                          indexBtn++) {
                                        if (indexBtn == index) {
                                          isSelected[indexBtn] = true;
                                        } else {
                                          isSelected[indexBtn] = false;
                                        }
                                      }
                                    });
                                  }),
                            )),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                // Button to close the ticket
                ConfirmationSlider(
                  text: 'Slide to close this ticket',
                  width: 400,
                  foregroundColor: kOrangeColorM,
                  foregroundShape: BorderRadius.circular(12),
                  backgroundShape: BorderRadius.circular(12),
                  textStyle: kTextStyle(18, Colors.white),
                  backgroundColor: kDarkBlueColorM,
                  onConfirmation: () {
                    confirmationBox(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
