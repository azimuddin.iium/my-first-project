import 'package:flutter/material.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';


// Confirmation to close
confirmationBox (context){
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        shape: RoundedRectangleBorder(         
          borderRadius: BorderRadius.circular(15.0)
        ),
        child: Container(
          decoration: kBoxDecorationM(radius: 15, color: Color(0xFFFCFCFC)),
          width: 400,
          height: 220,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                'Confirmation',
                style: kTextStyleBold(20, kDarkestBlueColorM),
                ),
                SizedBox(
                  height: 20,
              ),
              Text(
                'Are you sure want to close this ticket ?',
                style: kTextStyle(13, kDarkestBlueColorM),
                ),
                SizedBox(
                  height: 20,
              ),
              Divider(),
              InkWell(
                onTap: (){},
                child: Container(
                  height: 35,
                  child: Center(
                    child: Text(
                      'Close Ticket',
                       style: kTextStyleBold(16, Color(0xFF6E6F8E)),
                    ),
                  ),
                ),
              ),
              Divider(),
              InkWell(
                onTap: (){},
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      'Cancel',
                      style: kTextStyleBold(16, Color(0xFF6E6F8E)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      );
    }
  );
}