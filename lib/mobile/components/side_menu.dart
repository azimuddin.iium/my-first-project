import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:iium_technician/mobile/constants/constant_mobile.dart';

class SideMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: kDarkBlueColorM,
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 40),
                decoration: kBoxDecorationM(radius: 15, color: kDarkBlueColorM),
                child: Column(
                  children: [
                    Text(
                      'IIUM Tech',
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w900,
                          color: Colors.white),
                    )
                  ],
                ),
              ),
              Divider(
                height: 20,
                color: Colors.white,
              ),
              menuItem(
                text: 'Ticket History',
                svgAsset: 'assets/svg/history.svg',
                ontap: () {},
              ),
              SizedBox(height: 5),
              menuItem(
                text: 'F.A.Q', //tak sure nk implement ke tak
                svgAsset: 'assets/svg/faq.svg',
                ontap: () {},
              ),
              SizedBox(height: 5),
              menuItem(
                text: 'Notification', //tak sure nk implement ke tak
                svgAsset: 'assets/svg/ringing.svg',
                ontap: () {},
              ),
              Divider(
                height: 20,
                color: Colors.white,
              ),
              menuItem(
                text: 'Log Out',
                svgAsset: 'assets/svg/exit.svg',
                ontap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget menuItem({
    required String text,
    required String svgAsset,
    required VoidCallback ontap,
  }) {
    return ListTile(
      horizontalTitleGap: .0,
      contentPadding: EdgeInsets.symmetric(horizontal: 20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      leading: Container(
          child: SvgPicture.asset(
        svgAsset,
        width: 20,
        height: 20,
        color: kLightBackGroundColorM,
      )),
      title: Text(
        text,
        style: TextStyle(
          color: kLightBackGroundColorM,
          fontWeight: FontWeight.w500,
        ),
      ),
      onTap: ontap,
      hoverColor: Color(0xFF3A7ABA),
    );
  }
}
