import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';

class ItemTracker extends StatelessWidget {
  final String svgSrc, dataTracker, titleTracker;

  const ItemTracker({
    Key? key,
    required this.svgSrc,
    required this.dataTracker,
    required this.titleTracker,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 170,
      height: 100,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      margin: EdgeInsets.only(right: 10),
      decoration: kBoxDecorationM(radius: 10, color: kDarkBlueColorM).copyWith(
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 4),
            blurRadius: 4,
            color: Colors.grey.withOpacity(0.5),
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            child: SvgPicture.asset(
              svgSrc,
              width: 40,
              color: Colors.white,
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    dataTracker,
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  SizedBox(height: 5),
                  Text(
                    titleTracker,
                    overflow: TextOverflow.clip,
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
