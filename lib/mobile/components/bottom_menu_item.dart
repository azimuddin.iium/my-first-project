import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';

class ButtonMenuItem extends StatelessWidget {
  final VoidCallback onPressed;
  final int _selectedIndex;
  final int itemIndex;
  final String itemTitle;
  final String iconText;

  const ButtonMenuItem({
    Key? key,
    required selectedIndex,
    required this.onPressed,
    required this.itemIndex,
    required this.itemTitle,
    required this.iconText,
  })  : _selectedIndex = selectedIndex,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 9,
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: 50,
              height: 5,
              decoration: kBoxDecorationM(
                radius: 5,
                color: _selectedIndex == itemIndex
                    ? kDarkBlueColorM
                    : Colors.white,
              )),
          IconButton(
            splashColor: Colors.transparent,
            hoverColor: Colors.transparent,
            onPressed: onPressed,
            icon: SvgPicture.asset(iconText,
                color: _selectedIndex == itemIndex
                    ? kDarkBlueColorM
                    : Color(0xFF3A7ABA)),
          ),
          Text(
            _selectedIndex == itemIndex ? itemTitle : '',
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
          )
        ],
      ),
    );
  }
}
