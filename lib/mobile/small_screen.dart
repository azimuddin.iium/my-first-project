import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:iium_technician/mobile/components/bottom_menu_item.dart';
import 'package:iium_technician/mobile/components/side_menu.dart';
import 'package:iium_technician/mobile/constants/constant_mobile.dart';
import 'package:iium_technician/mobile/screens/home_screen.dart';
import 'package:iium_technician/mobile/screens/test_screen.dart';
import 'package:iium_technician/mobile/screens/ticket_screen.dart';

import 'package:iium_technician/mobile/screens/ticket_available_screen.dart';

// Bottom Navigation Bar

class SmallScreen extends StatefulWidget {
  const SmallScreen({Key? key}) : super(key: key);

  @override
  _SmallScreenState createState() => _SmallScreenState();
}

class _SmallScreenState extends State<SmallScreen> {
  bool isActive = false;
  int _selectedIndex = 0;

  List<Widget> screen = [
    HomeScreen(),
    TicketScreen(),
    TicketAvalaible(),
    //TestScreen(),
  ];

  void onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    //Wrapper for mobile screen size

    return Scaffold(
      drawer: SideMenu(),
      appBar: AppBar(
        backgroundColor: kDarkBlueColorM,
        elevation: 0,
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        child: SvgPicture.asset(
          'assets/svg/task.svg',
          width: 35,
          color: Colors.white,
        ),
        backgroundColor: kOrangeColorM,
        onPressed: () {
          onItemTapped(1);
        },
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(horizontal: 60),
        width: double.infinity,
        height: 65,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -7),
              blurRadius: 33,
              color: Color(0xFF6DAED9).withOpacity(0.16),
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ButtonMenuItem(
              itemTitle: 'Home',
              iconText: 'assets/svg/home.svg',
              selectedIndex: _selectedIndex,
              onPressed: () {
                onItemTapped(0);
              },
              itemIndex: 0,
            ),
            ButtonMenuItem(
              itemTitle: 'Settings',
              iconText: 'assets/svg/settings.svg',
              selectedIndex: _selectedIndex,
              onPressed: () {
                onItemTapped(2);
              },
              itemIndex: 2,
            ),
          ],
        ),
      ),
      body: NotificationListener<SwitchBody>(
          onNotification: (SwitchBody index) {
            onItemTapped(index.val);

            return false;
          },
          child: screen[_selectedIndex]),
    );
  }
}

// Change body when some button is pressed

class SwitchBody extends Notification {
  final int val;
  SwitchBody(this.val);
}
