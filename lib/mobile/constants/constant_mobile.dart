import 'package:flutter/material.dart';

const kDarkBlueColorM = Color(0xFF285480);
const kLightBackGroundColorM = Color(0xFFF8FBFF);
const kOrangeColorM = Color(0xFFFF715A);
const kLightBlueColorM = Color(0xFFE2F3FF);
const kDarkestBlueColorM = Color(0xFF10124D);
const kLightGreenColorM = Color(0xFF96FF45);

TextEditingController remarkController = TextEditingController();

List<String> kuliyyahListM = [
  'Centre for Languages & Pre-University Academic Development (CELPAD)',
  'Ahmad Ibrahim Kulliyyah of Laws (AIKOL)',
  'Kulliyyah of Architecture & Environmental Design (KAED)',
  'Kulliyyah of Economics & Management Sciences (KENMS)',
  'Kulliyyah of Education (KOED)',
  'Kulliyyah of Engineering (KOE)',
  'Kulliyyah of Information & Communication Technology (KICT)',
  'Kulliyyah of Islamic Reveal Knowledge And Human Sciences (KIRKHS)'
];

List<String> kuliyyahCodeListM = [
  'CELPAD',
  'AIKOL',
  'KAED',
  'KENMS',
  'KOED',
  'KOE',
  'KICT',
  'KIRKHS'
];

BoxDecoration kBoxDecorationM({required double radius, required Color color}) {
  return BoxDecoration(
    color: color,
    boxShadow: [
      BoxShadow(
        color: Colors.grey.withOpacity(0.3),
        spreadRadius: 1,
        blurRadius: 1,
        offset: Offset(0, 1), // changes position of shadow
      ),
    ],
    border: Border.all(color: Colors.transparent),
    borderRadius: BorderRadius.all(
      Radius.circular(radius),
    ),
  );
}

BoxDecoration kBoxDecorationBottomM(double radius, Color color) {
  return BoxDecoration(
    color: color,
    border: Border.all(color: Colors.transparent),
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(radius),
      bottomRight: Radius.circular(radius),
    ),
  );
}

TextStyle kTextStyleBold(double size, Color color) {
  return TextStyle(
    fontSize: size,
    color: color,
    fontWeight: FontWeight.bold,
  );
}

TextStyle kTextStyle(double size, Color color) {
  return TextStyle(
    fontSize: size,
    color: color,
  );
}

TextStyle kTextStyleUnderline(double size, Color color) {
  return TextStyle(
      fontSize: size, color: color, decoration: TextDecoration.underline);
}
