import 'package:flutter/material.dart';
import 'package:iium_technician/helpers/resposiveness.dart';
import 'package:iium_technician/desktop_web/large_screen.dart';
import 'package:iium_technician/mobile/constants/screen_config.dart';
import 'package:iium_technician/mobile/small_screen.dart';

class SiteLayout extends StatelessWidget {
  const SiteLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      body: ResposiveWidget(
        largeScreen: LargeScreen(),
        smallScreen: SmallScreen(),
      ),
    );
  }
}
